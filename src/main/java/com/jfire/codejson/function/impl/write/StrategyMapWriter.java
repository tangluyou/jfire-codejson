package com.jfire.codejson.function.impl.write;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import com.jfire.baseutil.collection.StringCache;
import com.jfire.codejson.function.JsonWriter;
import com.jfire.codejson.function.WriterAdapter;
import com.jfire.codejson.function.impl.write.wrapper.StringWriter;
import com.jfire.codejson.strategy.WriteStrategy;

public class StrategyMapWriter extends WriterAdapter
{
    private WriteStrategy strategy;
    private JsonWriter    stringWriter;
    
    public StrategyMapWriter(WriteStrategy strategy)
    {
        this.strategy = strategy;
        stringWriter = strategy.getWriter(String.class);
        if (stringWriter instanceof StringWriter)
        {
            stringWriter = null;
        }
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    public void write(Object target, StringCache cache)
    {
        cache.append('{');
        Set<Entry> set = ((Map) target).entrySet();
        for (Entry each : set)
        {
            if (each.getKey() != null && each.getValue() != null)
            {
                
                if (each.getKey() instanceof String)
                {
                    if (stringWriter == null)
                    {
                        cache.append('"').append((String) each.getKey()).append("\":");
                    }
                    else
                    {
                        stringWriter.write(each.getKey(), cache);
                        cache.append(':');
                    }
                }
                else
                {
                    cache.append('"');
                    strategy.getWriter(each.getKey().getClass()).write(each.getKey(), cache);
                    cache.append("\":");
                }
                if (each.getValue() instanceof String)
                {
                    if (stringWriter == null)
                    {
                        cache.append('"').append((String) each.getValue()).append('"');
                    }
                    else
                    {
                        stringWriter.write(each.getKey(), cache);
                    }
                }
                else
                {
                    strategy.getWriter(each.getValue().getClass()).write(each.getValue(), cache);
                }
                cache.append(',');
            }
        }
        if (cache.isCommaLast())
        {
            cache.deleteLast();
        }
        cache.append('}');
    }
}
